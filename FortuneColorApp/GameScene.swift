//
//  GameScene.swift
//  SwiftDelegateTrial
//
//  Created by okuno on 2016/04/21.
//  Copyright (c) 2016年 okuno. All rights reserved.
//

import SpriteKit

class GameScene: SKScene {
    
    var delegate_escape: SceneEscapeProtocol?
    
    let titleLabel = SKLabelNode(fontNamed: "Verdana-bold")
    let startLabel = SKLabelNode(fontNamed: "Verdana-bold")
    
    override func didMoveToView(view: SKView) {
        
        self.backgroundColor = UIColor.whiteColor()
        let defaultFontColor = UIColor.darkGrayColor()
        
        titleLabel.text = "今日はなにいろ？"
        titleLabel.fontSize = 35
        titleLabel.fontColor = defaultFontColor
        titleLabel.position = CGPoint(x: CGRectGetMidX(self.frame), y: CGRectGetMidY(self.frame))
        self.addChild(titleLabel)
        
        
        startLabel.text = "はじめる"
        startLabel.fontSize = 25
        startLabel.fontColor = defaultFontColor
        startLabel.position = CGPoint(x: CGRectGetMidX(self.frame), y: 200)
        self.addChild(startLabel)
        
    }
    
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
        
        for touch in touches {
            
            let location = touch.locationInNode(self)
            let touchNode = self.nodeAtPoint(location)

            if touchNode == startLabel {
                print("touched the start label");
                delegate_escape!.sceneEscape(self)
            } else {
                print("touched somewhere");
            }
        }
    }
 
    override func update(currentTime: CFTimeInterval) {
        /* Called before each frame is rendered */
    }
}
