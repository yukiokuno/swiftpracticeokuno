//
//  GameViewController.swift
//  SwiftDelegateTrial
//
//  Created by okuno on 2016/04/21.
//  Copyright (c) 2016年 okuno. All rights reserved.
//

import UIKit
import SpriteKit
import Foundation

class GameViewController: UIViewController, SceneEscapeProtocol {
    
    var skView: SKView?

    override func viewDidLoad() {
        super.viewDidLoad()

        // Configure the view.
        self.skView = self.view as? SKView
        skView!.showsFPS = true
        skView!.showsNodeCount = true
        skView!.ignoresSiblingOrder = true
        goGame()
    }
    
    func goGame() {
        let gameScene: GameScene = GameScene(size: CGSizeMake(1024, 768))
        gameScene.delegate_escape = self
        gameScene.scaleMode = SKSceneScaleMode.AspectFill
        self.skView!.presentScene(gameScene)
    }
    
    func goSelect(){
        let selectScene: SelectScene = SelectScene(size: CGSizeMake(1024, 768))
        selectScene.delegate_escape = self
        selectScene.scaleMode = SKSceneScaleMode.AspectFill
        self.skView!.presentScene(selectScene)

    }
    
    func goResult(){
        let resultScene: ResultScene = ResultScene(size: CGSizeMake(1024, 768))
        resultScene.delegate_escape = self
        resultScene.scaleMode = SKSceneScaleMode.AspectFill
        self.skView!.presentScene(resultScene)
    }
    
    func sceneEscape(scene: SKScene) {
        if scene.isKindOfClass(GameScene) {
            goSelect()
        } else if scene.isKindOfClass(SelectScene) {
            goResult()
        } else if scene.isKindOfClass(ResultScene){
            goGame()
        }
    }
    
    override func shouldAutorotate() -> Bool {
        return true
    }

    override func supportedInterfaceOrientations() -> UIInterfaceOrientationMask {
        if UIDevice.currentDevice().userInterfaceIdiom == .Phone {
            return .AllButUpsideDown
        } else {
            return .All
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Release any cached data, images, etc that aren't in use.
    }

    override func prefersStatusBarHidden() -> Bool {
        return true
    }
}
