//
//  ResultScene.swift
//  SwiftDelegateTrial
//
//  Created by okuno on 2016/04/21.
//  Copyright © 2016年 okuno. All rights reserved.
//

import SpriteKit
import WeatherReporter

class ResultScene: SKScene {
    
    var delegate_escape: SceneEscapeProtocol?

    let titleLabel = SKLabelNode(fontNamed: "Verdana-bold")
    let cityLabel = SKLabelNode(fontNamed: "Verdana-bold")
    let weatherLabel = SKLabelNode(fontNamed: "Verdana-bold")
    let exitLabel = SKLabelNode(fontNamed: "Verdana-bold")
    let redImg = SKSpriteNode(imageNamed: "red.png")
    let yellowImg = SKSpriteNode(imageNamed: "yellow.png")
    let blueImg = SKSpriteNode(imageNamed: "blue.png")
    let greenImg = SKSpriteNode(imageNamed: "green.png")
    let defaultFontColor = UIColor.darkGrayColor()
    var city = "Tokyo"
    var weatherCondition = "-"
    var temp = "-"
    
    override func didMoveToView(view: SKView) {
    
        getWeatherInfo()
        
        self.backgroundColor = UIColor.whiteColor()
        
        let red = ("あか", redImg)
        let yellow = ("きいろ", yellowImg)
        let blue = ("あお", blueImg)
        let green = ("みどり", greenImg)
        
        let colors = [red, yellow, blue, green]
        let randomNum = (Int)(arc4random_uniform(4))
        let selectedColor = colors[randomNum]

        
        titleLabel.text = "今日は" + selectedColor.0
        titleLabel.fontSize = 35
        titleLabel.fontColor = defaultFontColor
        titleLabel.position = CGPoint(x: CGRectGetMidX(self.frame), y: 680)
        self.addChild(titleLabel)
        
        let colorImg = selectedColor.1
        colorImg.position = CGPoint(x: CGRectGetMidX(self.frame), y: CGRectGetMidY(self.frame))
        self.addChild(colorImg)
        
        cityLabel.text = self.city
        cityLabel.fontSize = 20
        cityLabel.fontColor = defaultFontColor
        cityLabel.position = CGPoint(x: CGRectGetMidX(self.frame), y: 250)
        self.addChild(cityLabel)
        
        self.weatherLabel.text = "Today's Weather: " + self.weatherCondition + " / " + self.temp + "℃"
        self.weatherLabel.fontSize = 15
        self.weatherLabel.fontColor = self.defaultFontColor
        self.weatherLabel.position = CGPoint(x: CGRectGetMidX(self.frame), y: 200)
        self.addChild(self.weatherLabel)
        
        exitLabel.text = "おわる"
        exitLabel.fontSize = 25
        exitLabel.fontColor = defaultFontColor
        exitLabel.position = CGPoint(x: CGRectGetMidX(self.frame), y: 100)
        self.addChild(exitLabel)
        
    }
    
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
        
        for touch in touches {
            
            let location = touch.locationInNode(self)
            let touchNode = self.nodeAtPoint(location)
            if touchNode == exitLabel {
                print("touched the start label");
                delegate_escape!.sceneEscape(self)
            } else {
                print("touched somewhere");
            }
        }
    }
    
    //get weather information and replace the label
    func getWeatherInfo() {
        let weatherReporter = WeatherReporter(appId: "22a47e842d644721227d21d5325b7388")
        
        weatherReporter.setCity(self.city)
        
        weatherReporter.getReport(){
            (data, error) -> Void in
            if error != nil {
                print(error)
            } else {
                print(data)
                let reportedWeather = data["weather"] as! [[String : AnyObject]]
                let descriptionIndex = reportedWeather[0].indexForKey("description")
                let descriptionObj = (reportedWeather[0])[descriptionIndex!]
                self.weatherCondition = descriptionObj.1 as! String
                
                let reportedMainWeather = data["main"] as! NSDictionary
                self.temp = (String)(reportedMainWeather["temp"] as! NSInteger - 273)
                
                let removingNodes : [SKNode] = [self.weatherLabel]
                self.removeChildrenInArray(removingNodes)
                self.weatherLabel.text = "Today's Weather: " + self.weatherCondition + " / " + self.temp + "℃"
                self.weatherLabel.fontSize = 15
                self.weatherLabel.fontColor = self.defaultFontColor
                self.weatherLabel.position = CGPoint(x: CGRectGetMidX(self.frame), y: 200)
                self.addChild(self.weatherLabel)
            }
        }
    }

    override func update(currentTime: CFTimeInterval) {
        /* Called before each frame is rendered */
    }
}
