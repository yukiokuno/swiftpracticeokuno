//
//  SceneEscapeProtocol.swift
//  SwiftDelegateTrial
//
//  Created by okuno on 2016/04/21.
//  Copyright © 2016年 okuno. All rights reserved.
//

import SpriteKit

protocol SceneEscapeProtocol {
    func sceneEscape(scene: SKScene)
}
