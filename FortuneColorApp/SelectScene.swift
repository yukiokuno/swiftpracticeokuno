//
//  SelectScene.swift
//  SwiftDelegateTrial
//
//  Created by okuno on 2016/04/21.
//  Copyright © 2016年 okuno. All rights reserved.
//


import SpriteKit

class SelectScene: SKScene {
    
    var delegate_escape: SceneEscapeProtocol?
    
    let titleLabel = SKLabelNode(fontNamed: "Verdana-bold")
    let firstOptionLabel = SKLabelNode(fontNamed: "Verdana-bold")
    let secondOptionLabel = SKLabelNode(fontNamed: "Verdana-bold")
    let thirdOptionLabel = SKLabelNode(fontNamed: "Verdana-bold")
    let fourthOptionLabel = SKLabelNode(fontNamed: "Verdana-bold")
    
    override func didMoveToView(view: SKView) {
        
        self.backgroundColor = UIColor.whiteColor()
        let defaultFontColor = UIColor.darkGrayColor()
        
        titleLabel.text = "1つえらんでね？"
        titleLabel.fontSize = 35
        titleLabel.fontColor = defaultFontColor
        titleLabel.position = CGPoint(x: CGRectGetMidX(self.frame), y:680)
        self.addChild(titleLabel)
        
        var selectedOptions:[String] = getOptions()
        
        firstOptionLabel.text = selectedOptions[0]
        firstOptionLabel.fontSize = 35
        firstOptionLabel.fontColor = defaultFontColor
        firstOptionLabel.position = CGPoint(x: CGRectGetMidX(self.frame), y: 500)
        self.addChild(firstOptionLabel)

        secondOptionLabel.text = selectedOptions[1]
        secondOptionLabel.fontSize = 35
        secondOptionLabel.fontColor = defaultFontColor
        secondOptionLabel.position = CGPoint(x: CGRectGetMidX(self.frame), y: 420)
        self.addChild(secondOptionLabel)
        
        thirdOptionLabel.text = selectedOptions[2]
        thirdOptionLabel.fontSize = 35
        thirdOptionLabel.fontColor = defaultFontColor
        thirdOptionLabel.position = CGPoint(x: CGRectGetMidX(self.frame), y: 340)
        self.addChild(thirdOptionLabel)
        
        fourthOptionLabel.text = selectedOptions[3]
        fourthOptionLabel.fontSize = 35
        fourthOptionLabel.fontColor = defaultFontColor
        fourthOptionLabel.position = CGPoint(x: CGRectGetMidX(self.frame), y: 260)
        self.addChild(fourthOptionLabel)
        
    }
    
    func getOptions() -> [String] {
        var options = ["いんこ", "はげたか", "カメレオン", "はりねずみ", "くらげ", "ごじら", "ヒョウ", "きりん", "アルパカ", "ふくろう", "シカ", "ミジンコ", "チンパンジー"]
        var selectedOptions:[String] = []
        for _ in 0...3{
            let optionAmount:UInt32 = (UInt32)(options.count)
            let randomNum = (Int)(arc4random_uniform(optionAmount))
            selectedOptions.append(options[randomNum])
            options.removeAtIndex(randomNum)
        }
        return selectedOptions
    }
    
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
        
        for touch in touches {
            
            let location = touch.locationInNode(self)
            let touchNode = self.nodeAtPoint(location)
            
            print(location.x)
            print(location.y)

            
            if touchNode == firstOptionLabel || touchNode == secondOptionLabel || touchNode == thirdOptionLabel || touchNode == fourthOptionLabel{
                print("touched the start label");
                delegate_escape!.sceneEscape(self)
            } else {
                print("touched somewhere");
            }
        }
    }
    
    override func update(currentTime: CFTimeInterval) {
        /* Called before each frame is rendered */
    }
}
