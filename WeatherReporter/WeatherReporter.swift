//
//  WeatherReporter.swift
//  FortuneColor
//
//  Created by okuno on 2016/04/28.
//  Copyright © 2016年 okuno. All rights reserved.
//

import Foundation
public class WeatherReporter {

    let appId : String
    var city : String
    
    public init(appId: String) {
        self.appId = appId
        self.city = ""
    }
    
    public func setCity(city: String) {
        self.city = city
    }
    
    public func getReport(callback: (NSDictionary, String?) -> Void) {
        
        print("enterted sdk httpGet method")
        
        let session = NSURLSession.sharedSession()
        let urlString = "http://api.openweathermap.org/data/2.5/weather?q=" + self.city + "&APPID=" + self.appId
        let request = NSMutableURLRequest(URL: NSURL(string: urlString)!)
        
        // set the method(HTTP-GET)
        request.HTTPMethod = "GET"
        
        // use NSURLSessionDataTask
        let task = session.dataTaskWithRequest(request){
            (data, response, error) -> Void in
            let emptyDictionary = NSDictionary()
            if error != nil {
                callback(emptyDictionary, error?.localizedDescription)
            } else {
                do{
                    let result = try NSJSONSerialization.JSONObjectWithData(data!, options: NSJSONReadingOptions.AllowFragments) as! NSDictionary
                    print("result in sdk")
                    print(result)
                    callback(result, nil)
                }catch let encodingError as NSError {
                    print(encodingError)
                    callback(emptyDictionary, encodingError.localizedDescription)
                }
            }
        }
        task.resume()
    }
    
}

